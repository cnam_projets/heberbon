from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .models import MachineVirtuelles, Hebergement, VirtuelDesktop
from .forms import ServeurPropre, FormCreationHebergement, FormCreationBureau
from . import forms, models


@login_required

# # # # # # Creation machine virtuelle # # # # # #
def creation_machine_virtuelle(request):
    machine_form = forms.FormMachineVirtuelles()
    if request.method == "POST":
        machine_form = forms.FormMachineVirtuelles(request.POST)
        if machine_form.is_valid():

            # Générer objet machine
            machine = machine_form.save(commit=False)

            # Asigne l'utilisateur de la BDD avec l'utilisateur current
            machine.utilisateur = request.user
            machine.save()
            return redirect("home/")

    return render(request, "machine_virtuelles/creation_machine_virtuelle.html", {"machine_form": machine_form})

# show list to machines virtuelles
def show_list_machines_virtuelles (request):
    # Prendre l'objet utilisateur et compare avec l'utilisateur current
    # Et Affiche seuelement les données d'utilisateur current
    list_machines_virtuelles = ServeurPropre.objects.filter(utilisateur=request.user)

    return render(request, 'machine_virtuelles/list_machines_virtuelles.html', {'list_machines_virtuelles': list_machines_virtuelles})

# # # # # # Creation hebergement # # # # # #
def creation_hebergement(request):
    hebergement_form = forms.FormCreationHebergement()
    if request.method == "POST":
        hebergement_form = forms.FormCreationHebergement(request.POST)
        if hebergement_form.is_valid():

            # Générer objet hebergement
            hebergement = hebergement_form.save(commit=False)

            # Asigne l'utilisateur de la BDD avec l'utilisateur current
            hebergement.utilisateur = request.user
            hebergement.save()
            return redirect("home/")

    return render(request, "machine_virtuelles/creation_hebergement.html", {"hebergement_form": hebergement_form})

# show list to Hebergement
def show_list_hebergement (request):
    # Prendre l'objet utilisateur et compare avec l'utilisateur current
    # Et Affiche seuelement les données d'utilisateur current
    list_hebergements = Hebergement.objects.filter(utilisateur=request.user)

    return render(request, 'machine_virtuelles/list_hebergements.html', {'list_hebergements': list_hebergements})


# # # # # # Creation Bureau # # # # # #
def creation_environnement_de_bureau(request):
    environnement_de_bureau_form = forms.FormCreationBureau()
    if request.method == "POST":
        environnement_de_bureau_form = forms.FormCreationBureau(request.POST)
        if environnement_de_bureau_form.is_valid():

            # Générer objet hebergement
            environnement_de_bureau = environnement_de_bureau_form.save(commit=False)

            # Asigne l'utilisateur de la BDD avec l'utilisateur current
            environnement_de_bureau.utilisateur = request.user
            environnement_de_bureau.save()
            return redirect("home/")

    return render(request, "machine_virtuelles/creation_environnement_de_bureau.html", {"environnement_de_bureau_form": environnement_de_bureau_form})

# show list to Hebergement
def show_list_environnement_de_bureau (request):
    # Prendre l'objet utilisateur et compare avec l'utilisateur current
    # Et Affiche seuelement les données d'utilisateur current
    list_environnement_de_bureau = VirtuelDesktop.objects.filter(utilisateur=request.user)

    return render(request, 'machine_virtuelles/list_environnement_de_bureau.html', {'list_environnement_de_bureau': list_environnement_de_bureau})

# Test page hui

def hui (request):
    return render(request, "machine_virtuelles/hui.html")
