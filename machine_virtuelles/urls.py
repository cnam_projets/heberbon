from django.urls import path
from . import views


urlpatterns = [
    # creation_machine_virtuelle
    path('creation_machine_virtuelle/', views.creation_machine_virtuelle,
    name='creation_machine_virtuelle'),

    # Liste de la visualisation list_machines_virtuelles
    path('list_machines_virtuelles/', views.show_list_machines_virtuelles, name='list_machines_virtuelles'),

    # creation_hebergement
    path('creation_hebergement/', views.creation_hebergement, name='creation_hebergement'),

    # Liste de la visualisation list_Hebergements
    path('list_hebergements/', views.show_list_hebergement, name='list_hebergements'),

    # creation_environnement_de_bureau
    path('creation_environnement_de_bureau/', views.creation_environnement_de_bureau, name='creation_environnement_de_bureau'),

    # Liste de la visualisation list_environnement_de_bureau
    path('list_environnement_de_bureau/', views.show_list_environnement_de_bureau, name='list_environnement_de_bureau'),

    # Hui
    path('hui', views.hui, name='hui'),
    ]
