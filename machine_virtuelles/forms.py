from django import forms
from .models import ServeurPropre, Hebergement, VirtuelDesktop

class FormMachineVirtuelles(forms.ModelForm):
    class Meta:
        model = ServeurPropre
        fields = ["nom", "nomDomaine", "cpu", "ram", "stockage", "systeme",]
        labels = {"nom": "Name", "nomDomaine": "Nom de domaine",}

class FormCreationHebergement (forms.ModelForm):
    class Meta:
        model = Hebergement
        fields = ["nom", "nomDomaine", "cpu", "ram", "stockage", "systeme","typeBdd", "serveur",]
        labels = {"typeBdd": "Le type de BDD",}

class FormCreationBureau (forms.ModelForm):
    class Meta:
        model = VirtuelDesktop
        fields = ["nom", "nomDomaine", "cpu", "ram", "stockage", "systeme","desktopEnvironment",]
        labels = {"desktopEnvironment": "Desktop"}
