from django.apps import AppConfig


class MachineVirtuellesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'machine_virtuelles'
