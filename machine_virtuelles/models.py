from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from systemes.models import Systeme


class MachineVirtuelles(models.Model):
    nom = models.CharField(max_length=50)
    nomDomaine = models.CharField(max_length=50)
    cpu = models.IntegerField()
    ram = models.IntegerField()
    stockage = models.IntegerField()
    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    systeme = models.ForeignKey(Systeme, on_delete=models.CASCADE)


class Hebergement(MachineVirtuelles):
    serveur = models.CharField(max_length=50)
    typeBdd = models.CharField(max_length=50)

class ServeurPropre(MachineVirtuelles):
    typeDeploiemment = models.CharField(max_length=50)

class VirtuelDesktop(MachineVirtuelles):
    desktopEnvironment = models.CharField(max_length=50)


