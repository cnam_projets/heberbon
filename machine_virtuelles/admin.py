from django.contrib import admin
from .models import MachineVirtuelles , Hebergement, ServeurPropre, VirtuelDesktop
# Register your models here.
admin.site.register(MachineVirtuelles)
admin.site.register(Hebergement)
admin.site.register(ServeurPropre)
admin.site.register(VirtuelDesktop)
