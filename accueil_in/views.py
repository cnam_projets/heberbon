from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from machine_virtuelles.models import MachineVirtuelles
from users.models import User
@login_required
def home(request):
    return render(request, 'accueil_in/home.html')

