from django.apps import AppConfig


class AccueilInConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'accueil_in'
