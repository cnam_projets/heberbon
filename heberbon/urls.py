"""
URL configuration for heberbon project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import users.views
import accueil.views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('connexion/', users.views.login_page, name='connexion'),
    path('logout/', users.views.logout_user, name='logout'),
    path('signup/', users.views.signup_page, name='signup'),
    path('', accueil.views.la_primaire_page_accueil, name='la_primaire_page_accueil'),
    # The views includes
    path('accueil/', include('accueil.urls')),
    path('accueil_in/', include('accueil_in.urls')),
    path('machine_virtuelles/', include('machine_virtuelles.urls')),
    path('run_machines/', include('run_machines.urls')),
    path('noms_domaines/', include('noms_domaines.urls')),
]
