from django.apps import AppConfig


class NomsDomainesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'noms_domaines'
