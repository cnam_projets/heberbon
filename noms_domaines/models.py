from django.db import models
from django.conf import settings
#from django_dnf.fields import DomainNameField



class NomDomaine (models.Model):
    nom_domaine = models.CharField(max_length=50)
    #nom_domaine = DomainNameField(blank=True)
    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
