from django import forms
from .models import NomDomaine

class FormNomDomaine(forms.ModelForm):
    class Meta:
        model = NomDomaine
        fields = ["nom_domaine",]
        labels = {"nom_domaine": "nom_domaine",}
