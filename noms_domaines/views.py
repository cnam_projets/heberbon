from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .models import NomDomaine
from .forms import FormNomDomaine
from . import forms, models

def creation_nom_domaine (request):
    nom_domaine_form = forms.FormNomDomaine()
    if request.method == "POST":
        nom_domaine_form = forms.FormNomDomaine(request.POST)
        if nom_domaine_form.is_valid():

            # Générer objet domaine
            domaine = nom_domaine_form.save(commit=False)

            # Asigne l'utilisateur de la BDD avec l'utilisateur current
            domaine.utilisateur = request.user
            domaine.save()
            return redirect("home/")

    return render(request, "noms_domaines/creation_nom_domaine.html", {"nom_domaine_form": nom_domaine_form})

# montre les noms domaines
def show_list_noms_domaines (request):
    # Prendre l'objet utilisateur et compare avec l'utilisateur current
    # Et Affiche seuelement les données d'utilisateur current
    list_noms_domaines = NomDomaine.objects.filter(utilisateur=request.user)

    return render(request, 'noms_domaines/list_noms_domaines.html', {'list_noms_domaines': list_noms_domaines})
