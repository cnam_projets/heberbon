from django.urls import path
from . import views

urlpatterns = [
    path('creation_nom_domaine/', views.creation_nom_domaine,
    name='creation_nom_domaine'),
    path('list_noms_domaines/', views.show_list_noms_domaines,
    name='list_noms_domaines'),
    ]
