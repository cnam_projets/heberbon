from django.shortcuts import render
from django.http import HttpResponseRedirect


def la_primaire_page_accueil (request):

    return render(request, 'les_pages_de_contenu/la_primaire_page_accueil.html')

def qui_somme_nous (request):

    return render(request, 'les_pages_de_contenu/qui_somme_nous.html')
