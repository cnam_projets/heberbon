from django.urls import path
from . import views


urlpatterns = [
    # creation_machine_virtuelle
    path('la_primaire_page_accueil/', views.la_primaire_page_accueil, name='la_primaire_page_accueil'),
    path('qui_somme_nous/', views.qui_somme_nous, name='qui_somme_nous'),

    ]
