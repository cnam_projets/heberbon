from django.db import models
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    # Les champs supplémentaires
    #account_number_phone = models.IntegerField(unique=True)
    profile_photo = models.ImageField(verbose_name='Photo de profil')
