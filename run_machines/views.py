from django.shortcuts import render
from django.http import HttpResponseRedirect
from machine_virtuelles.models import ServeurPropre, Hebergement, VirtuelDesktop

#
def run_machine (request):

    name_run_machine = ServeurPropre.objects.values_list('nom')

    return render(request, "run_machines/run_machines.html", {'name_run_machine': name_run_machine})

#
def run_hebergement(request):

    name_run_hebergement = Hebergement.objects.values_list('nom')

    return render(request, "run_machines/run_hebergement.html", {'name_run_hebergement': name_run_hebergement})

#
def run_environnement_de_bureau(request):

    name_run_environnement_de_bureau = VirtuelDesktop.objects.values_list('nom')

    return render(request, "run_machines/run_environnement_de_bureau.html", {'name_run_environnement_de_bureau': name_run_environnement_de_bureau})
