from django.urls import path
from . import views

urlpatterns = [
    path('run_machine/', views.run_machine, name='run_machine'),
    path('run_hebergement/', views.run_hebergement, name='run_hebergement'),
    path('run_environnement_de_bureau/', views.run_environnement_de_bureau, name='run_environnement_de_bureau')
    ]
