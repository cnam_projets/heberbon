from django.apps import AppConfig


class SystemesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'systemes'
