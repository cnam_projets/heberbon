from django.db import models

class Systeme (models.Model):
    nomSysteme = models.CharField(max_length=50)
    hostName = models.CharField(max_length=50)
    nomUtilisateurSysteme = models.CharField(max_length=50)
    mdpUtilisateurSysteme = models.CharField(max_length=50)


class PackageSysteme (models.Model):
    nomPackage = models.CharField(max_length=50)
    systeme = models.ForeignKey(Systeme, on_delete=models.CASCADE)
