from django.contrib import admin
from .models import Systeme, PackageSysteme

admin.site.register(Systeme)
admin.site.register(PackageSysteme)
